define(['custom'], function(custom, app) {
	var fileName  = 'footer';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $modal, cfpLoadingBar, stBlurredDialog, SmoothScroll, toaster, uiModalCtrl) {	   
				    

					
					//////////////////////
					$scope.open = function () {			
							stBlurredDialog.open();
							var control = uiModalCtrl.browserInfoCtrl(); 
							
							
							
							var modalInstance = $modal.open({
						      	templateUrl: 'browserInfoModal.html',
						  		controller: control,
							    resolve: {
							        data: function () {
							        // return $scope.deleteUserObj;
							        }
							    }
							});
						
						
							modalInstance.result.then(
								function (returnData) {  // CLOSE																											
									stBlurredDialog.close();
									custom.logger('Modal dismissed at: ' + new Date());
									}, 
								function () {			// DISMISS	
									stBlurredDialog.close();
						  			custom.logger('Modal dismissed at: ' + new Date());
						    });	
						
					};
					//////////////////////					
					
					
					$scope.lorem = "Lorem ipsum dolor sit amet, an epicuri mediocrem vituperata eos. Illud volumus periculis per no, cu torquatos definitionem eum. Est cu hendrerit vituperatoribus. In officiis nominati eum, aperiri dolorem usu ut. In meliore denique suavitate vim.";
				   
				   	$scope.copyRight = "Copyright: All content on this web site, such as text, graphics, logos, button icons and images is the property of MyDreamSite, L.L.C. or its content suppliers and protected by United States and international copyright laws. The selection, arrangement, and presentation of all materials on this web site (including information in the public domain), and the overall design of this web site is the exclusive property of MyDreamSite, L.L.C. and protected by United States and international copyright laws. All software on this site is the property of MyDreamSite, L.L.C. or its software suppliers and protected by United States and international copyright laws."
				   	
				    
				});
				

				
				
				
							
								
	    },
	    ///////////////////////////////////////
  };
});
